const express=require('express')
const cors=require('express')
const router=require('../router')
const app=express()
app.use(cors('*'))
app.use(express.json())
app.use('/person', router)

app.listen(3000, '0.0.0.0', ()=>
{
    console.log("server get strated at 3000")
})